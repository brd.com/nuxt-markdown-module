const path = require('path')
const FastGlob = require('fast-glob');

const MODULE_OPTIONS = {
  // default glob matches against all markdown files in the pages directory:
  glob: ['pages/**/*.md'],

  // default path just maps to no `pages` prefix and drop suffixes of `index`.
  pathTransform: (path) => '/' + path.match(/pages\/(.*?).md$/)[1].replace(/index$/g,''),
  nameTransform: (path) => undefined,
  markdownit: {},
}

function pathJoin(items) {
  return items.join('/').replace(/\/\//g,'/').replace(/\/\//g,'/');
}

function insertRoute(routeObject,name,path,component,pathPrefix="") {
  const fullPath = pathJoin([pathPrefix,routeObject.path].filter(a => a && a.length))

  if(path.indexOf(fullPath+'/') != 0 || routeObject.path == '/') {
    // console.log(`INSERT ROUTE ${path} into ${fullPath} - nerrp`);
    return false;
  }

  // console.log(`INSERT ROUTE ${path} into ${fullPath} (${routeObject.path}) - yepp`);

  let children = Array.isArray(routeObject) ? routeObject : routeObject.children;
  if(children) {
    for(var i in children) {
      var c = children[i];

      // insertRoute returns true if it inserted it:
      if(insertRoute(c,name,path,component,fullPath)) {
        // console.log("inserted into child");
        return true;
      }
    }
  }

  if(!children) {
    children = routeObject.children = [];
  }

  // remove fullPath prefix - paths are always specified relative to the parent:
  let trimmedPath = path.substring(fullPath.length);

  if(trimmedPath.length > 1 && trimmedPath.indexOf('/') == 0) {
    trimmedPath = trimmedPath.substring(1);
  }

  // console.log("Pushing in trimmed path ",trimmedPath);
  children.push({
    // name,
    path: trimmedPath,
    component,
  });

  return true;
}

module.exports = function (moduleOptions) {
  const { glob,
    pathTransform,
    nameTransform,
    markdownit,
   } = { ...MODULE_OPTIONS , ...moduleOptions };

  this.extendRoutes((routes,resolve) => {
    const files = FastGlob.sync(glob,{ cwd: this.nuxt.options.srcDir });

    files.forEach(f => {
      const path = pathTransform(f);
      const name = nameTransform(f);
      const component = resolve(this.nuxt.options.srcDir,f);

      insertRoute(routes,name,path,component);
    })
  });

  this.extendBuild((config) => {
    config.module.rules.push(
      {
        test: /\.md$/,
        use: [
          {
            loader: 'vue-loader'
          },
          {
            loader: 'vue-markdown-loader/lib/markdown-compiler',
            options: markdownit
          }
        ]
      }
    )
  });

  console.log("Added hooks...");
};
